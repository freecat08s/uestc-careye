//
// Created by PulsarV on 18-5-10.
//

#ifndef ROBOCAR_RC_GLOBAL_WHEEL_H
#define ROBOCAR_RC_GLOBAL_WHEEL_H
//wheel serial commond
#define RC_WHEEL_1_FORWARD "010000000000000"
//[1轮方向，1轮速度，2轮方向，2轮速度，3轮方向，3轮速度]
//    1      1000     0      0000      0      0000
#define RC_WHEEL_1_BACKWARD "110000000000000"

#define RC_WHEEL_2_FORWARD "000001100000000"
#define RC_WHEEL_2_BACKWARD "000000100000000"
#define RC_WHEEL_3_FORWARD "000000000001000"
#define RC_WHEEL_3_BACKWARD "000000000011000"

#define RC_WHEEL_GO_FORWARD "000000030000300"
#define RC_WHEEL_GO_BACKWARD "000001030010300"

#define RC_WHEEL_CW "002001020000200"
#define RC_WHEEL_AC "102000020010200"

#define RC_WHEEL_STOP "000000000000000"

#define RC_MOVE_DEVICE_PORT_INITATION_ERROR (char *) "You should be initation the camera index before start"
#define RC_OPEN_CAMERA_ERROR (char *) "Can not open such device or video"

#endif //ROBOCAR_RC_GLOBAL_WHEEL_H
