//
// Created by pulsarv on 2021/7/6.
//

#ifndef FOLLOW_DOWN_TRACKER_EKF_H
#define FOLLOW_DOWN_TRACKER_EKF_H

#define Nsta 2     // 9 state values: pressure, temperature
#define Mobs 4     // 18 measurements: baro pressure, baro temperature, LM35 temperature

#include<rc_controller/TinyEKF.h>
#include<opencv2/opencv.hpp>


namespace follow_down {
    namespace control {
        class tracker_ekf : public TinyEKF {
        public:
            tracker_ekf();

            ~tracker_ekf();

        protected:
            void model(double fx[Nsta], double F[Nsta][Nsta], double hx[Mobs], double H[Mobs][Nsta]) override;
        };
    }
}


#endif //FOLLOW_DOWN_TRACKER_EKF_H
