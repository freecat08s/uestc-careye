//
// Created by Pulsar on 2020/5/18.
//

#ifndef UESTC_CAREYE_MATCONVERT_H
#define UESTC_CAREYE_MATCONVERT_H

#include <string>
#include <opencv2/opencv.hpp>
namespace rccore {
    namespace computer_version {
        std::string base64Decode(const char *Data, int DataByte);


        std::string base64Encode(const unsigned char *Data, int DataByte);

        //imgType 包括png bmp jpg jpeg等opencv能够进行编码解码的文件
        std::string Mat2Base64(const cv::Mat &img, std::string imgType);


        cv::Mat Base2Mat(std::string &base64_data);

    }
}

#endif //UESTC_CAREYE_MATCONVERT_H
