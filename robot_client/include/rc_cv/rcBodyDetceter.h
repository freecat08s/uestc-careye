//
// Created by Pulsar on 2020/5/16.
//

#ifndef UESTC_CAREYE_RCBODYDETCETER_H
#define UESTC_CAREYE_RCBODYDETCETER_H

#include <string>
#include <vector>
#include <opencv2/opencv.hpp>
namespace RC {
    namespace CV {
        class BodyDetceter {
        public:
            int init_body_cascade(std::string file_path);

            std::vector <cv::Rect> detcetBody(cv::Mat src);

        private:
            cv::CascadeClassifier body_cascade;
        };
    }
}


#endif //UESTC_CAREYE_RCBODYDETCETER_H
