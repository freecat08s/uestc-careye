//
// Created by Pulsar on 2020/5/30.
//

#include <rc_message/rc_gyro_msg.h>
namespace rccore {
    namespace message {
        GyroMessage::GyroMessage(int max_queue_size) : BaseMessage<data_struct::CJY901>(max_queue_size) {
        }

        GyroMessage::~GyroMessage() {

        }
    }
}