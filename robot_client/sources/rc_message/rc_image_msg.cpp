//
// Created by PulsarV on 18-10-30.
//

#include <rc_message/rc_base_msg.hpp>
#include <map>
#include <rc_message/rc_image_msg.h>
#include <rc_cv/MatConvert.h>

namespace rccore {
    namespace message {
        ImageMessage::ImageMessage(int max_queue_size) : BaseMessage<cv::Mat>(max_queue_size) {

        }

        std::string ImageMessage::front_base64_string_message() {
            return computer_version::Mat2Base64(this->front_message(), "png");
        }
    }
}