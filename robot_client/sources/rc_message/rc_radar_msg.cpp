//
// Created by PulsarV on 18-10-30.
//

#include <rc_message/rc_radar_msg.h>

namespace rccore {
    namespace message {
        RadarMessage::RadarMessage(int max_queue_size)
                : BaseMessage<std::vector<rccore::data_struct::DOT>>(max_queue_size) {
        }

        RadarMessage::~RadarMessage() {

        }
    }
}